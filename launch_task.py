from celery import Celery

BROKER_URL = 'redis://localhost:6379/0'
BACKEND_URL = 'redis://localhost:6379/1'

app = Celery('tasks', broker=BROKER_URL, backend=BACKEND_URL)

_task = app.signature('basic_worker.add', (20, 22))
_res = _task.delay()
print(_res.get())
#...  if not isinstance(v, (ResultBase, tuple))]

